package com.cooge.colletion.deal;

import com.cooge.colletion.pojo.TbContent;
import com.cooge.colletion.pojo.TbUser;
import com.cooge.colletion.service.TbContentService;
import com.cooge.colletion.service.TbUserService;
import com.cooge.colletion.util.DataFactory;

public class Control {
	
	private static TbContentService  tbContentService = (TbContentService) DataFactory.getFactory(TbContentService.class);
	
	private static TbUserService  tbUserService = (TbUserService) DataFactory.getFactory(TbUserService.class);
	
	
	public static void saveTbContent(TbContent tbContent){
		
		tbContentService.add(tbContent);
	}
	public static TbUser getUserByUsername(String userName){
		
		return 	tbUserService.getUserByUserName(userName);
	}

}
