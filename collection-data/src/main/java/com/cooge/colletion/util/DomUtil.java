package com.cooge.colletion.util;

import java.util.Iterator;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class DomUtil {

	public static Elements Domanalysis(Document doc, String s[]) {

		return Domanalysis(doc.body(), s);
	}

	public static Elements Domanalysis(Elements ele, String s[]) {

		if (s[0].startsWith(".")) {

			Iterator<Element> em = ele.iterator();
			while (em.hasNext()) {

				Element el = em.next();
				ele = el.getElementsByClass(s[0].substring(1));

				if (s.length == 1) {

					return ele;
				}

				String ss[] = new String[s.length - 1];

				System.arraycopy(s, 1, ss, 0, ss.length);

				return Domanalysis(ele, ss);

			}

		} else {

			Iterator<Element> em = ele.iterator();
			while (em.hasNext()) {

				Element el = em.next();
				ele = el.getElementsByTag(s[0]);

				if (s.length == 1) {

					return ele;
				}

				String ss[] = new String[s.length - 1];

				System.arraycopy(s, 1, ss, 0, ss.length);
				return Domanalysis(ele, ss);

			}

		}
		return ele;
	}

	public static Elements Domanalysis(Element el, String s[]) {
		
		if(s.length==0||(s.length==1&&s[0].equals(""))){
			
			
			return new Elements();
		}

		if (s[0].startsWith(".")) {

			Elements ele = el.getElementsByClass(s[0].substring(1));

			if (s.length == 1) {

				return ele;
			}

			String ss[] = new String[s.length - 1];

			System.arraycopy(s, 1, ss, 0, ss.length);

			return Domanalysis(ele, ss);
		} else {
			Elements ele = el.getElementsByTag(s[0]);

			if (s.length == 1) {

				return ele;
			}
			String ss[] = new String[s.length - 1];

			System.arraycopy(s, 1, ss, 0, ss.length);
			return Domanalysis(ele, ss);

		}

	}

}
