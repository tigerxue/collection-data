package com.cooge.colletion.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.cooge.colletion.model.Content;

public class FileUtil {
	
	private static String PATH = "data/";
	
	public static String[] getFilelist(){
		
		File file = new File(PATH);
		
		return file.list();
		
	}
	
	public static boolean Save(Serializable s,String filename){
		
		try {
			
			File  f = new File(PATH);
			if(!f.isDirectory()){
				f.mkdirs();
			}
			System.out.println(f.getAbsolutePath());
			FileOutputStream fs = new FileOutputStream(PATH+filename);
			ObjectOutputStream os =  new ObjectOutputStream(fs);
			os.writeObject(s);
			os.close();
			fs.close();
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return false;
		
	}
	public static  Object read(String filename){
		
		try {
			FileInputStream fs = new FileInputStream(PATH+filename);
			ObjectInputStream ois = new ObjectInputStream(fs);
			Object  o =  ois.readObject();
			ois.close();
			fs.close();
			return o;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
