package com.cooge.colletion.util;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

public class DataFactory{
	
	   private static  ApplicationContext act = null;
	
	   public static void init(){
		   
		   if(act==null)
		    act=new FileSystemXmlApplicationContext("classpath:spring-config.xml");
		   
	   }
	   @SuppressWarnings("unchecked")
	public static Object getFactory(Class c){
		   
		   return DataFactory.act.getBean(c);
	   }
}
