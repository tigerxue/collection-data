package com.cooge.colletion.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cooge.colletion.dao.TbContentDao;
import com.cooge.colletion.pojo.TbContent;
import com.cooge.colletion.service.TbContentService;
@Service
public class TbContentServiceImpl  implements TbContentService{
	
	@Autowired
	TbContentDao tbContentDao;
	

	public TbContent add(TbContent tbContent) {
		
		return tbContentDao.add(tbContent);
	}

}
