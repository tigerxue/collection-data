package com.cooge.colletion.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cooge.colletion.dao.TbUserDao;
import com.cooge.colletion.pojo.TbUser;
import com.cooge.colletion.service.TbUserService;
@Service
public class TbUserServiceImpl implements TbUserService {
	
	@Autowired
	TbUserDao tbUserDao;

	public TbUser getUserByUserName(String userName) {
		
		TbUser u = new TbUser();
		u.setUserName(userName);
		try {
			u = tbUserDao.getList(u).get(0);
		} catch (Exception e) {
			u=null;
		}
		return u;
	}

}
