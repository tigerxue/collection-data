package com.cooge.colletion.service;

import com.cooge.colletion.pojo.TbUser;

public interface TbUserService {
	
	TbUser getUserByUserName(String userName);

}
