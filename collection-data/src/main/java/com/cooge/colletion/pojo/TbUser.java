package com.cooge.colletion.pojo;
// default package

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * TbUser entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "tb_User", schema = "dbo", catalog = "cyp123")
public class TbUser implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = 1791892940528855097L;
	private Integer id;
	private String userName;
	private String nickName;
	private String pwd;
	private String userLv;
	private Integer jiFeng;
	private String email;

	// Constructors

	/** default constructor */
	public TbUser() {
	}

	/** minimal constructor */
	public TbUser(Integer id, String userName) {
		this.id = id;
		this.userName = userName;
	}

	/** full constructor */
	public TbUser(Integer id, String userName, String nickName, String pwd,
			String userLv, Integer jiFeng, String email) {
		this.id = id;
		this.userName = userName;
		this.nickName = nickName;
		this.pwd = pwd;
		this.userLv = userLv;
		this.jiFeng = jiFeng;
		this.email = email;
	}

	// Property accessors
	@Id
	@Column(name = "ID", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "UserName", nullable = false)
	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "NickName")
	public String getNickName() {
		return this.nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	@Column(name = "pwd")
	public String getPwd() {
		return this.pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	@Column(name = "UserLV")
	public String getUserLv() {
		return this.userLv;
	}

	public void setUserLv(String userLv) {
		this.userLv = userLv;
	}

	@Column(name = "JiFeng")
	public Integer getJiFeng() {
		return this.jiFeng;
	}

	public void setJiFeng(Integer jiFeng) {
		this.jiFeng = jiFeng;
	}

	@Column(name = "Email")
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}