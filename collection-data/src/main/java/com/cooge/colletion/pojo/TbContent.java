package com.cooge.colletion.pojo;
// default package

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * TbContent entity. @author MyEclipse Persistence Tools
 */
@Entity
@Table(name = "tb_Content", schema = "dbo", catalog = "cyp123")
public class TbContent implements java.io.Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -1618282108099215316L;
	private Integer id;
	private String titleName;
	private String sendContent;
	private Timestamp subTime;
	private Integer isPassed;
	private Integer dingCount;
	private Integer caiCount;
	private Integer passCount;
	private Integer unpassCount;
	private Integer contentType;
	private Integer userId;
	private String fileName;

	// Constructors

	/** default constructor */
	public TbContent() {
	}

	/** minimal constructor */
	public TbContent(Integer id) {
		this.id = id;
	}

	/** full constructor */
	public TbContent(Integer id, String titleName, String sendContent,
			Timestamp subTime, Integer isPassed, Integer dingCount,
			Integer caiCount, Integer passCount, Integer unpassCount,
			Integer contentType, Integer userId, String fileName) {
		this.id = id;
		this.titleName = titleName;
		this.sendContent = sendContent;
		this.subTime = subTime;
		this.isPassed = isPassed;
		this.dingCount = dingCount;
		this.caiCount = caiCount;
		this.passCount = passCount;
		this.unpassCount = unpassCount;
		this.contentType = contentType;
		this.userId = userId;
		this.fileName = fileName;
	}

	// Property accessors
	@Id
	@Column(name = "ID", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "TitleName")
	public String getTitleName() {
		return this.titleName;
	}

	public void setTitleName(String titleName) {
		this.titleName = titleName;
	}

	@Column(name = "SendContent")
	public String getSendContent() {
		return this.sendContent;
	}

	public void setSendContent(String sendContent) {
		this.sendContent = sendContent;
	}

	@Column(name = "SubTime", length = 23)
	public Timestamp getSubTime() {
		return this.subTime;
	}

	public void setSubTime(Timestamp subTime) {
		this.subTime = subTime;
	}

	@Column(name = "isPassed")
	public Integer getIsPassed() {
		return this.isPassed;
	}

	public void setIsPassed(Integer isPassed) {
		this.isPassed = isPassed;
	}

	@Column(name = "DingCount")
	public Integer getDingCount() {
		return this.dingCount;
	}

	public void setDingCount(Integer dingCount) {
		this.dingCount = dingCount;
	}

	@Column(name = "CaiCount")
	public Integer getCaiCount() {
		return this.caiCount;
	}

	public void setCaiCount(Integer caiCount) {
		this.caiCount = caiCount;
	}

	@Column(name = "PassCount")
	public Integer getPassCount() {
		return this.passCount;
	}

	public void setPassCount(Integer passCount) {
		this.passCount = passCount;
	}

	@Column(name = "UnpassCount")
	public Integer getUnpassCount() {
		return this.unpassCount;
	}

	public void setUnpassCount(Integer unpassCount) {
		this.unpassCount = unpassCount;
	}

	@Column(name = "ContentType")
	public Integer getContentType() {
		return this.contentType;
	}

	public void setContentType(Integer contentType) {
		this.contentType = contentType;
	}

	@Column(name = "UserID")
	public Integer getUserId() {
		return this.userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	@Column(name = "fileName")
	public String getFileName() {
		return this.fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

}