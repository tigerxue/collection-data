package com.cooge.colletion.model;

public class Content implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1084473404522332292L;
	private String title;
	private String img;
	private String text;
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getImg() {
		return img;
	}
	public void setImg(String img) {
		this.img = img;
	}
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	@Override
	public String toString() {
		String str = "[title]="+this.getTitle()+" [img]="+this.getImg()+" [text]="+this.text;
		return str;
	}

}
