package com.cooge.colletion.model;

public class Rx implements java.io.Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 557877077481086249L;
	//开始页
	private Integer start;
	//结束页
	private Integer end;
	//记录标题关键词
	private String[]  nav;
	//记录网址
	private String rxUrl;
	//分页标签
	private String programaTag;
	//标题标签
	private String titleTag;
	//图片标签
	private String imgTag;
	//文本标签
	private String textTag;
	//抓取规则
	private String rule;
	//用户名
	private String userName;

	public String getTitleTag() {
		return titleTag;
	}

	public void setTitleTag(String titleTag) {
		this.titleTag = titleTag;
	}

	public String getImgTag() {
		return imgTag;
	}

	public void setImgTag(String imgTag) {
		this.imgTag = imgTag;
	}

	public String getRxUrl() {
		return rxUrl;
	}

	public void setRxUrl(String rxUrl) {
		this.rxUrl = rxUrl;
	}

	public String getProgramaTag() {
		return programaTag;
	}

	public void setProgramaTag(String programaTag) {
		this.programaTag = programaTag;
	}

	public String[] getNav() {
		return nav;
	}

	public void setNav(String[] nav) {
		this.nav = nav;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getEnd() {
		return end;
	}

	public void setEnd(Integer end) {
		this.end = end;
	}

	public String getRule() {
		return rule;
	}

	public void setRule(String rule) {
		this.rule = rule;
	}

	public String getTextTag() {
		return textTag;
	}

	public void setTextTag(String textTag) {
		this.textTag = textTag;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

}
