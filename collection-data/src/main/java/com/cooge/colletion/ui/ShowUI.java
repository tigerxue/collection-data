package com.cooge.colletion.ui;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import com.cooge.colletion.deal.Control;
import com.cooge.colletion.model.Content;
import com.cooge.colletion.model.Rx;
import com.cooge.colletion.pojo.TbContent;
import com.cooge.colletion.pojo.TbUser;
import com.cooge.colletion.util.DataFactory;
import com.cooge.colletion.util.FileUtil;
import com.cooge.colletion.util.HtmlUtil;

public class ShowUI extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6856710527073204152L;
	private JPanel contentPane;
	private JTextField programaTag;
	private JTextField titleTag;
	private JTextField imgTag;
	private JTextField rxUrl;
	private JComboBox<String> comboBox;
	private JTextField page;
	private JTextField ruleText;
	private JEditorPane navEdit;
	private JTextField textTag;
	private JTextField userName;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		
		DataFactory.init();
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ShowUI frame = new ShowUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ShowUI() {
		setTitle("图片类信息抓取");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 613, 329);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton button = new JButton("开始抓取数据");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Rx r = new Rx();
				
				r.setImgTag(imgTag.getText());
				
				r.setProgramaTag(programaTag.getText());
				
				r.setTitleTag(titleTag.getText());
				
				r.setRxUrl(rxUrl.getText());
				r.setTextTag(textTag.getText());
				
				String [] strs = navEdit.getText().split("\n");
				
				r.setNav(strs);
				
				r.setRule(ruleText.getText());
				
				String []ps = page.getText().split("-");
				
				r.setStart(Integer.valueOf(ps[0]));
				
				r.setEnd(Integer.valueOf(ps[1]));
				int i =Integer.valueOf(ps[0]);
				int ii = Integer.valueOf(ps[1]);
				TbUser tUser = Control.getUserByUsername(userName.getText()); 
				while(true){
					if(i>ii)break;
					List<Content> clist = 	HtmlUtil.getContent(String.valueOf(i),strs[0], r);
					for(Content c:clist){
						
						if(c.getImg().trim().equals(""))continue;
						
						TbContent tbContent =new TbContent();
						tbContent.setTitleName(c.getTitle());
						tbContent.setSendContent(c.getText());
						tbContent.setFileName(c.getImg());
						tbContent.setUserId(tUser.getId());
						tbContent.setSubTime(new Timestamp(new Date().getTime()));
						tbContent.setIsPassed(1);
						tbContent.setCaiCount(0);
						tbContent.setDingCount(5);
						tbContent.setContentType(0);
						tbContent.setUnpassCount(0);
						tbContent.setPassCount(0);
						Control.saveTbContent(tbContent);
						
					}
					
					i++;
				}
			}
		});
		button.setBounds(452, 257, 122, 23);
		contentPane.add(button);
		
		comboBox = new JComboBox<String>();
		comboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange()==1){
						try {
							rxUrl.setText("");
							programaTag.setText("");
							imgTag.setText("");
							titleTag.setText("");
							navEdit.setText("");
							ruleText.setText("");
							page.setText("");
							Rx r = (Rx) FileUtil.read( e.getItem().toString());
							rxUrl.setText(r.getRxUrl());
							programaTag.setText(r.getProgramaTag());
							imgTag.setText(r.getImgTag());
							titleTag.setText(r.getTitleTag());
							textTag.setText(r.getTextTag());
							userName.setText(r.getUserName());
							
						String[] st = 	r.getNav();
						String n = "";
						 for(int i = 0;i<st.length;i++){
							 
							 n=n+st[i];
							 if(i!=st.length-1){
								 n=n+'\n';
							 }
						 }
						 navEdit.setText(n);
						 ruleText.setText(r.getRule());
						 page.setText(r.getStart()+"-"+r.getEnd());
						} catch (Exception e2) {
							e2.printStackTrace();
						}
						
					
				}
				
			}
		});

		comboBox.setBounds(74, 10, 170, 21);
		contentPane.add(comboBox);
		
		new Thread(new Runnable(){

			public void run() {
				setComboBox();
			}}).start();
		
		JLabel label = new JLabel("网址选择");
		label.setBounds(10, 13, 54, 15);
		contentPane.add(label);
		
		programaTag = new JTextField();
		programaTag.setBounds(74, 71, 170, 21);
		contentPane.add(programaTag);
		programaTag.setColumns(10);
		
		JLabel label_1 = new JLabel("分栏标签");
		label_1.setBounds(10, 74, 54, 15);
		contentPane.add(label_1);
		
		JLabel label_2 = new JLabel("标题标签");
		label_2.setBounds(47, 164, 54, 15);
		contentPane.add(label_2);
		
		titleTag = new JTextField();
		titleTag.setBounds(103, 161, 141, 21);
		contentPane.add(titleTag);
		titleTag.setColumns(10);
		
		JLabel label_3 = new JLabel("图片标签");
		label_3.setBounds(47, 192, 54, 15);
		contentPane.add(label_3);
		
		imgTag = new JTextField();
		imgTag.setBounds(103, 189, 141, 21);
		contentPane.add(imgTag);
		imgTag.setColumns(10);
		
		JLabel label_4 = new JLabel("当前网址");
		label_4.setBounds(10, 49, 54, 15);
		contentPane.add(label_4);
		
		rxUrl = new JTextField();
		rxUrl.setBounds(73, 46, 170, 21);
		contentPane.add(rxUrl);
		rxUrl.setColumns(10);
		
		JButton button_1 = new JButton("新增");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				Rx r = new Rx();
				
				r.setImgTag(imgTag.getText());
				
				r.setProgramaTag(programaTag.getText());
				
				r.setTitleTag(titleTag.getText());
				
				r.setRxUrl(rxUrl.getText());
				
				String [] strs = navEdit.getText().split("\n");
				
				r.setNav(strs);
				
				r.setRule(ruleText.getText());
				
				String []ps = page.getText().split("-");
				
				r.setStart(Integer.valueOf(ps[0]));
				
				r.setEnd(Integer.valueOf(ps[1]));
				
				r.setTextTag(textTag.getText());
				r.setUserName(userName.getText());
				
				FileUtil.Save(r, rxUrl.getText().substring(rxUrl.getText().indexOf("/")+2, rxUrl.getText().length()-1));
				
				
				
			}
		});
		button_1.setBounds(151, 220, 93, 23);
		contentPane.add(button_1);
		
		JLabel label_5 = new JLabel("标题关键词");
		label_5.setBounds(254, 13, 77, 15);
		contentPane.add(label_5);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(341, 10, 233, 93);
		contentPane.add(scrollPane);
		
		navEdit = new JEditorPane();
		scrollPane.setViewportView(navEdit);
		
		page = new JTextField();
		page.setBounds(324, 111, 250, 21);
		contentPane.add(page);
		page.setColumns(10);
		
		JLabel label_7 = new JLabel("抓取规则");
		label_7.setBounds(254, 144, 54, 15);
		contentPane.add(label_7);
		
		ruleText = new JTextField();
		ruleText.setBounds(324, 141, 250, 21);
		contentPane.add(ruleText);
		ruleText.setColumns(10);
		
		JButton button_2 = new JButton("删除");
		button_2.setBounds(57, 220, 93, 23);
		contentPane.add(button_2);
		
		JLabel lblw = new JLabel("<html>\r\n[w]表示网址\r\n<br>\r\n[T]表示标题\r\n<br>\r\n[p]表示页数\r\n</html>");
		lblw.setBounds(334, 169, 240, 58);
		contentPane.add(lblw);
		
		JLabel lblNewLabel = new JLabel("页数");
		lblNewLabel.setBounds(272, 114, 36, 15);
		contentPane.add(lblNewLabel);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(47, 266, 255, 14);
		contentPane.add(progressBar);
		
		JLabel label_6 = new JLabel("文本标签");
		label_6.setBounds(47, 129, 54, 15);
		contentPane.add(label_6);
		
		textTag = new JTextField();
		textTag.setBounds(103, 129, 141, 21);
		contentPane.add(textTag);
		textTag.setColumns(10);
		
		userName = new JTextField();
		userName.setBounds(103, 102, 141, 21);
		contentPane.add(userName);
		userName.setColumns(10);
		
		JLabel label_8 = new JLabel("用户名");
		label_8.setBounds(47, 102, 46, 15);
		contentPane.add(label_8);
	}
	public void setComboBox(){
		
		String [] ss = FileUtil.getFilelist();
		for(String str:ss){
			comboBox.addItem(str);
		}
	}
}
