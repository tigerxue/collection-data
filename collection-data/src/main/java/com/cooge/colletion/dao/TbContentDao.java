package com.cooge.colletion.dao;

import java.util.List;

import com.cooge.colletion.pojo.TbContent;

public interface TbContentDao extends B<TbContent> {
	
	TbContent add(TbContent TbContent);
	
	TbContent findById(Integer TbContentId);
	
	List<TbContent> findlist(TbContent tbContent);

}
