package com.cooge.colletion.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.cooge.colletion.dao.BaseDao;
import com.cooge.colletion.dao.TbContentDao;
import com.cooge.colletion.pojo.TbContent;
@Repository
public class TbContentDaoImpl extends BaseDao<TbContent> implements TbContentDao  {

	@Override
	public Class<TbContent> getEntityClass() {
		return TbContent.class;
	}

	public TbContent add(TbContent TbContent) {
		return this.save(TbContent);
	}

	public TbContent findById(Integer TbContentId) {
		return this.get(TbContentId);
	}

	public List<TbContent> findlist(TbContent tbContent) {
		return this.getList(tbContent);
	}

}
