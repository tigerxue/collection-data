package com.cooge.colletion.dao;

import java.util.List;

public interface B<T> {
	
	public T get(Integer id) ;
	public List<T> getlist();
	public List<T> getList(T t);
	public T save(T t);
	public List<T> getPageList(T t,int first,int max);
	public List<T> getPageList(int first,int max);
}
