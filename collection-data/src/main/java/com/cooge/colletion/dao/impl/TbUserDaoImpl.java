package com.cooge.colletion.dao.impl;

import org.springframework.stereotype.Repository;

import com.cooge.colletion.dao.BaseDao;
import com.cooge.colletion.dao.TbUserDao;
import com.cooge.colletion.pojo.TbUser;
@Repository
public class TbUserDaoImpl extends BaseDao<TbUser> implements TbUserDao {

	@Override
	public Class<TbUser> getEntityClass() {
		return TbUser.class;
	}


}
