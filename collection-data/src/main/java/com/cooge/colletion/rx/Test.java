package com.cooge.colletion.rx;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.cooge.colletion.model.Content;

public class Test {

	public static void main(String[] args) {
		String url = "http://www.0824.com/";
		List<Content> clist = new ArrayList<Content>();
		Document doc = null;
		try {
			doc = Jsoup.connect(url).get();
		} catch (IOException e) {

			e.printStackTrace();
		}
		Elements  eles = doc.getElementsByClass("LstItem");
		
		
		 ListIterator<Element> el =  eles.listIterator();
		
		while(el.hasNext()){
			
			Element e =  el.next();
			
			Content c =new Content();
			c.setTitle(e.getElementsByClass("titl").first().getElementsByTag("a").text());
			c.setImg(e.getElementsByClass("desc").first().getElementsByTag("img").text());
			clist.add(c);
			
			
		}
		
	}

}
